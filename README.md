SandboxApp
----------

This application exists to provide a testing stable testing environment for HaydenPierce\ClassFinder.
It should not be required by any other package.